<?php
// Routes

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/scoring', function ($request, $response, $args) {
    $files = $request->getUploadedFiles();
    $body = $request->getParsedBody();
    if (empty($files['newfile']) || empty($body['wordnet'])) {
        $error = array(
            'error' => ''
        );
        if(empty($files['newfile'])){
            $error['error'] = $error['error'] . 'Tidak ada file penilaian. ';
        }
        if(empty($body['wordnet'])){
            $error['error'] = $error['error'] . 'Tentukan apakah anda menggunakan WordNet. ';
        }
        $newResponse = $response->getBody()->write(json_encode($error));
        return $response->withStatus(400);
    }

    $newfile = $files['newfile'];

	if ($newfile->getError() === UPLOAD_ERR_OK) {
        $grader = $this->get('grader');
    	$uploadFileName = $newfile->getClientFilename();
    	$newfile->moveTo("scoring.json");
        $string = file_get_contents("scoring.json");
        $scores = json_decode($string, true);
        $result = $grader->grade($scores, $body['wordnet']);
        return $response->getBody()->write(json_encode($result));
	}
    // do something with $newfile
});