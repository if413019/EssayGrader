<?php

namespace Tests\AutomationGrader;

/**
 * This is an example class that shows how you could set up a method that
 * runs the application. Note that it doesn't cover all use-cases and is
 * tuned to the specifics of this skeleton app, so if your needs are
 * different, you'll need to change it.
 */
class WordNet 
{

    private $term;
    private $termSynset;
    private $value;
    private $idf;
    private $jawaban;
    private $jawabanIdf;
    private $con;

    public function __construct(){
    }

    // public function calculate($wordBags){
    //     $this->term = array();
    //     $this->value = array();
    //     $this->idf = array();
    //     $this->getTerm($wordBags);
    //     sort($this->term);
    //     $this->calculateTf($wordBags);
    //     $this->calculateIdf($wordBags);
    //     $this->calculateTfIdf();
    //     return $this->value;
    // }

    public function getSynonim($kunciJawaban, $jawaban){
        set_time_limit(3600);
        $this->con = mysql_connect("localhost", "root", "");
        if (!$this->con) {
            die("database connect problem");
        }
        $db_use = mysql_select_db("kamus", $this->con) or die("selet db problem !!");
        $this->term = array();
        $this->value = array();
        $this->idf = array();
        $this->termSynset = array();
        $this->getTerm($kunciJawaban);
        sort($this->term);
        $this->fillTermSynset();
        $this->calculateTf($kunciJawaban);
        $newKunciJawaban = $this->findSynsetKunciJawaban($kunciJawaban);
        // $this->termSynset();
        // print_r($this->term);
        // print_r($jawaban);
        $newJawaban = $this->findSynset($jawaban);
        // print_r($this->termSynset);
        // die();
        return array(
            $newKunciJawaban,
            $newJawaban
        );
    }

    public function fillTermSynset(){
        foreach($this->term as $term){
            array_push($this->termSynset, $this->getTermSynset($term));
        }
    }

    public function findSynsetKunciJawaban($kunciJawaban){
        for($i = 0; $i < count($kunciJawaban); $i++){
                for($a = 0; $a < count($this->term); $a++){
                    if($this->value[$i][$a] > 0){
                            for($k = 0; $k < count($kunciJawaban[$i]); $k++){
                                //echo ($this->term[$a] . ' ' . $jawaban[$i][$j][$k] . '<br>');
                                $temp = $this->getTermSynset($kunciJawaban[$i][$k]);
                                if(!empty(array_intersect($temp, $this->termSynset[$a]))){
                                    $kunciJawaban[$i][$k] = $this->term[$a];
                                }
                            }
                    }
                }
            
        }
        return $kunciJawaban;
    }

    public function termSynset(){
        for($i = 0; $i < count($this->term)-1; $i++){
            for($j = $i+1; $j < count($this->term); $j++){
                $temp1Syn = $this->getTermSynset($this->term[$i]);
                $temp2Syn = $this->getTermSynset($this->term[$j]);
                if(!empty(array_intersect($temp1Syn, $temp2Syn))){
                    $this->term[$i] = $this->term[$j];
                }
            }
        }
        $this->term = $this->fixTerm($this->term);
    }

    public function fixTerm($term){
        $temp = array();
        foreach($term as $word){
            if(!in_array($word,$temp,true)){
                array_push($temp, $word);
            }
        }
        return $temp;
    }

    public function findSynset($jawaban){
        for($i = 0; $i < count($jawaban); $i++){
            for($j = 0; $j < count($jawaban[$i]); $j++){
                for($a = 0; $a < count($this->term); $a++){
                    if($this->value[$i][$a] > 0){
                            for($k = 0; $k < count($jawaban[$i][$j]); $k++){
                                //echo ($this->term[$a] . ' ' . $jawaban[$i][$j][$k] . '<br>');
                                $temp = $this->getTermSynset($jawaban[$i][$j][$k]);
                                if(!empty(array_intersect($temp, $this->termSynset[$a]))){
                                    $jawaban[$i][$j][$k] = $this->term[$a];
                                }
                            }
                    }
                }
            }
        }
        return $jawaban;
    }

    function getTermSynset($lemma){
        $temp = $this->getLemmaSynset($lemma);
        $tempSynset = array();
        try{
            while($item = mysql_fetch_array($temp, MYSQL_ASSOC)){
                array_push($tempSynset, $item['synset']);
            }
        } catch(Exception $e) {
            array_push($tempSynset, "0");
        }
        return $tempSynset;
    }

    function getLemmaSynset($lemma){
		$strQ = "select * from wordnet where lemma = '". $lemma . "' AND lang = 'B' AND (goodness = 'Y' OR goodness = 'O')";
		$resItem = $this->execQ($strQ);
		return $resItem;
	}

    function execQ($strQ){
		$res = mysql_query($strQ, $this->con);
		
		//echo $strQ;
		//echo mysql_errno($con) . ": " . mysql_error($con). "\n";
		return $res;		
	}

    public function getTerm($wordBags){
        foreach($wordBags as $wordBag){
            foreach($wordBag as $word){
                if(!in_array($word,$this->term,true)){
                    array_push($this->term, $word);
                }
            }
        }
    }

    public function calculateTf($wordBags){
        foreach($wordBags as $wordBag){
            $temp = array();
            foreach($this->term as $term){
                $value = 0;
                foreach($wordBag as $word){
                    if($term == $word){
                        $value++;
                    }
                }
                $value = $value/count($wordBag);
                array_push($temp, $value);
            }
            array_push($this->value, $temp);
        }
    }

}
